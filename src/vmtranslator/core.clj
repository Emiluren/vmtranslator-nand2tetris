(ns vmtranslator.core
  (:use [vmtranslator.parser]
        [vmtranslator.code-writer])
  (:require [clojure.string :as str])
  (:gen-class))

(use 'clojure.java.io)

(defn process-line [line wrtr]
  (when (not (or (.startsWith line "//") (.isEmpty line)))
    (.write wrtr (str "//" line "\n"))
    (let [parts (str/split line #" ")]
      (.write wrtr
              (case (command-type line)
                :arithmetic (write-arithmetic (arg1 line))
                :push (write-push (arg1 line) (arg2 line))
                :pop (write-pop (arg1 line) (arg2 line))
                :label (write-label (arg1 line))
                :goto (write-goto (arg1 line))
                :if (write-if (arg1 line))
                :call (write-call (arg1 line) (arg2 line))
                :return (write-return)
                :function (write-function (arg1 line) (arg2 line)))))))

(defn process-file [wrtr infile]
  (with-open [rdr (reader infile)]
    (set-filename (str/replace (last (str/split infile #"/")) #"\.vm" ""))
    (doseq [line (line-seq rdr)]
      (process-line (str/trim line) wrtr))))

(defn process-directory [wrtr dirname]
  (let [dir (clojure.java.io/file dirname)]
    (doseq [infile (file-seq dir)]
      (let [filepath (.getPath infile)]
        (when (.endsWith filepath ".vm")
          (process-file wrtr (.getPath infile)))))))

; TODO: support multiple source files
(defn -main
  "Translates a HACK vm program to HACK assembly."
  [& args]
  (let [infile (first args)
        outfile (if (.endsWith infile ".vm")
                  (str/replace infile #"\.vm" ".asm")
                  (str infile ".asm"))]
    (with-open [wrtr (writer outfile)]
      (.write wrtr (write-init))
      (if (.endsWith infile ".vm")
        (process-file wrtr infile)
        (process-directory wrtr infile)))))
