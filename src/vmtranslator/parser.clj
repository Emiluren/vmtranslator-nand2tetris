(ns vmtranslator.parser
  (:use [clojure.string :only [split]]))

(defn command-type [command]
  (case (first (split command #" "))
    "add" :arithmetic
    "sub" :arithmetic
    "neg" :arithmetic
    "eq" :arithmetic
    "gt" :arithmetic
    "lt" :arithmetic
    "and" :arithmetic
    "or" :arithmetic
    "not" :arithmetic

    "push" :push
    "pop" :pop

    "label" :label
    "goto" :goto
    "if-goto" :if
    "function" :function
    "return" :return
    "call" :call))

(defn arg1 [command]
  (let [parts (split command #" ")]
    (if (= (command-type command) :arithmetic)
      (first parts)
      (second parts))))

(defn arg2 [command]
  (bigdec (nth (split command #" ") 2)))
