(ns vmtranslator.code-writer)

(def bc 0) ; bool counter
(def rc 0) ; return counter

(def filename "undefined_file")
(defn set-filename [name]
  (def filename name))

(def function-name "dafun")

(def decrsp (str "@SP\n"
                 "M=M-1\n"))

(def pushd (str "@SP\n"
                "A=M\n"
                "M=D\n"
                "@SP\n"
                "M=M+1\n"))

(def popd (str decrsp
               "A=M\n"
               "D=M\n"))

(defn write-arithmetic [command]
  (let [pop2 (str "@SP\n"
                  "A=M-1\n"
                  "D=M\n"
                  "A=A-1\n")
        binop (fn [op] (str pop2 op decrsp))
        cmp (fn [op]
              (def bc (inc bc))
              (str pop2
                   "D=M-D\n"
                   "@CMP_OP_TRUE" bc "\n"
                   "D;" op "\n"
                   "D=0\n"
                   "@CMP_OP_END" bc "\n"
                   "0;JMP\n"
                   "(CMP_OP_TRUE" bc ")\n"
                   "D=-1\n"
                   "(CMP_OP_END" bc ")\n"
                   decrsp
                   "A=M-1\n"
                   "M=D\n"))]
   (case command
     "add" (binop "M=D+M\n")
     "sub" (binop "M=M-D\n")
     "neg" (str "@SP\n"
                "A=M-1\n"
                "M=-M\n")
     "eq" (cmp "JEQ")
     "gt" (cmp "JGT")
     "lt" (cmp "JLT")
     "and" (binop "M=D&M\n")
     "or" (binop "M=D|M\n")
     "not" (str "@SP\n"
                "A=M-1\n"
                "M=!M\n"))))

(defn write-push [segment index]
  (let [push-seg (fn [seg]
                   (str seg
                        "D=M\n"
                        "@" index "\n"
                        "A=D+A\n"
                        "D=M\n"
                        pushd))]
   (case segment
     "constant" (str "@" index "\n"
                     "D=A\n"
                     pushd)
     "local" (push-seg "@LCL\n")
     "argument" (push-seg "@ARG\n")
     "this" (push-seg "@THIS\n")
     "that" (push-seg "@THAT\n")
     "temp" (str "@R" (+ (bigdec index) 5) "\n"
                 "D=M\n"
                 pushd)
     "pointer" (str "@" (if (= index "0") "THIS" "THAT") "\n"
                    "D=M\n"
                    pushd)
     "static" (str "@" filename "." index "\n"
                   "D=M\n"
                   pushd)
     (str "YOLO: " segment " " index "\n"))))

(defn write-pop [segment index]
  (let [pop (str popd
                 "@R13\n"
                 "A=M\n"
                 "M=D\n")
        pop-seg (fn [seg]
                  (str "@" seg "\n"
                       "D=M\n"
                       "@" index "\n"
                       "D=D+A\n"
                       "@R13\n"
                       "M=D\n"
                       pop))]
    (case segment
      "local" (pop-seg "LCL")
      "argument" (pop-seg "ARG")
      "this" (pop-seg "THIS")
      "that" (pop-seg "THAT")
      "temp" (str popd
                  "@R" (+ index 5) "\n"
                  "M=D\n")
      "pointer" (str popd
                     "@" (if (= index 0) "THIS" "THAT") "\n"
                     "M=D\n")
      "static" (str popd
                    "@" filename "." index "\n"
                    "M=D\n"))))

(defn label-name [label]
  (str function-name "$" label))

(defn write-label [label]
  (str "(" (label-name label) ")\n"))

(defn write-goto [label]
  (str "@" (label-name label) "\n"
       "0;JMP\n"))

(defn write-if [label]
  (str popd
       "@" (label-name label) "\n"
       "D;JNE\n"))

(defn write-call [f argc]
  (let [push-address (fn [addr]
                       (str "@" addr "\n"
                            "D=M\n"
                            pushd))]
    (str "@return" (do (def rc (inc rc)) rc) "\n"
         "D=A\n"
         pushd
         (push-address "LCL")
         (push-address "ARG")
         (push-address "THIS")
         (push-address "THAT")

         "@SP\n" ; ARG = SP-n-5
         "D=M\n"
         "@" (+ argc 5) "\n"
         "D=D-A\n"
         "@ARG\n"
         "M=D\n"

         "@SP\n" ; LCL = SP
         "D=M\n"
         "@LCL\n"
         "M=D\n"

         "@" f "\n"
         "0;JMP\n"
         "(return" rc ")\n")))

(defn write-return []
  (let [restore-pointer (fn [pointer offset]
                      (str "@FRAME\n"
                           "D=M\n"
                           "@" offset "\n"
                           "A=D-A\n"
                           "D=M\n"
                           "@" pointer "\n"
                           "M=D\n"))]
    (str "@LCL\n" ; FRAME = LCL
         "D=M\n"
         "@FRAME\n"
         "M=D\n"

         (restore-pointer "RET" 5) ; RET = *(FRAME-5)

         popd ; *ARG = pop()
         "@ARG\n"
         "A=M\n"
         "M=D\n"

         "@ARG\n" ; SP = ARG + 1
         "D=M+1\n"
         "@SP\n"
         "M=D\n"

         (restore-pointer "THAT" 1)
         (restore-pointer "THIS" 2)
         (restore-pointer "ARG" 3)
         (restore-pointer "LCL" 4)

         "@RET\n"
         "A=M\n"
         "0;JMP\n")))

(defn write-function [f num-locals]
  (str "(" f ")\n"
       (apply str (repeat num-locals (write-push "constant" 0)))))

(defn write-init []
  (let [initv (fn [n v] (str "@" v "\n"
                             "D=A\n"
                             "@" n "\n"
                             "M=D\n"))]
    (str (initv "SP" 256)
         (initv "LCL" 300)
         (initv "ARG" 400)
         (write-call "Sys.init" 0))))
